'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = require('./config/lib/app');

app.start();