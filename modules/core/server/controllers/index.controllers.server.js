'use strict';

exports.home = function (req, res) {
    res.render('index', { title: 'Express', user: req.user });
};