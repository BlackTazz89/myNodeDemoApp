'use strict';

var index = require('../controllers/index.controllers.server');

module.exports = function (app) {
    app.route('/')
        .get(index.home);
};
