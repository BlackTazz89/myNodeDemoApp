'use strict';

var path = require('path'),
    log = require(path.resolve('./config/lib/logger')),
    async = require('async'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User');

exports.signup = function (req, res) {
    async.waterfall([
        function (done) {

            if (!req.body.firstName || !req.body.lastName || !req.body.username || !req.body.password || !req.body.email) {
                return done(new Error('Please provide required fields.'));
            }

            done();
        },
        // Save user
        function (done) {
            delete req.body.roles;

            var user = new User(req.body);
            user.displayName = user.firstName.trim() + ' ' + user.lastName.trim();

            user.save(function (err) {
                // Remove sensitive data before login
                user.password = undefined;
                user.salt = undefined;
                done(err, user);
            });
        },
        // Login
        function (user, done) {
            req.login(user, function (err) {
                done(err, user);
            });
        }

    ], function (err, user) {
        // Signup process failed
        if (err) {
            // Log the failure to signup
            log('error', 'User signup failed.', {
                error: err
            });

            res.render('register', {});
            return;
        }
        res.redirect('/');
    });
};

exports.signin = function (req, res, next) {

    passport.authenticate('local', function (err, user, info) {
        if (err || !user) {
            log('error', 'User signin failed.', {
                reason: 'Wrong credentials',
                error: err || null
            });
            res.status(400).send({
                message: 'Wrong credentials.'
            });
            return;
        }
        req.login(user, function (err) {
            if (err) {
                // Log the failure to signin
                log('error', 'User signin failed.', {
                    reason: 'Login error',
                    error: err
                });
                res.status(400).send({
                    message: 'Login error'
                });
                return;
            }
            res.redirect('/');
        });
    })(req, res, next);
};

exports.signupPage = function (req, res) {
    res.render('register', {});
};

exports.signinPage = function (req, res) {
    res.render('login', { user: req.user });
}

exports.signout = function (req, res) {
    req.logout();
    res.redirect('/');
};

exports.listAll = function (req, res, next) {
    User
        .find()
        .exec(function (err, results) {
            if (err != null) {
                res.status(500).send({
                    message: "There was an unexpected error during find all."
                })
                return;
            }
            res.send(results)
        })
}

exports.find = function (req, res, next) {
    User
        .find()
        .where('username').equals(req.params.username)
        .limit(5)
        .sort({ birthdate: -1 })
        .select('firstName lastName')
        .exec(function (err, results) {
            if (err != null) {
                res.status(500).send({
                    message: "There was an unexpected error during find."
                })
                return;
            }
            res.send(results)
        })
};

exports.createPage = function (req, res, next) {
    res.render('newUser', {});
};

exports.create = function (req, res, next) {
    delete req.body.roles;
    var user = new User(req.body)
    user.displayName = user.firstName.trim() + ' ' + user.lastName.trim();

    user.save(function (err) {
        if (err != null) {
            res.status(500).send({
                message: "There was an unexpected error while saving a new user."
            })
            return
        }
        res.send("User saved.")
    })
};

exports.guests = function (req, res, next) {
    res.send("Guests endpoint.");
};