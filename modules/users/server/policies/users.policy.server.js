'use strict';

var acl = require('acl');
acl = new acl(new acl.memoryBackend());

exports.invokeRolesPolicies = function () {
    acl.allow([{
        roles: ['admin'],
        allows: [{
            resources: '/users',
            permissions: ['get']
        }, {
            resources: '/users/create',
            permissions: ['get', 'post']
        }]
    }, {
        roles: ['user'],
        allows: [{
            resources: '/users/find/:username',
            permissions: ['get']
        }]
    }, {
        roles: ['guest'],
        allows: [{
            resources: '/users/guests',
            permissions: ['get']
        }]
    }]);

    acl.addRoleParents('user', 'guest');
    acl.addRoleParents('admin', 'user');
};

exports.isAllowed = function (req, res, next) {

    var roles = (req.user && req.user.roles) ? req.user.roles : ['guest'];
    acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
        if (err) {
            return res.status(500).json({
                message: 'Unexpected authorization error'
            });
        } else {
            if (isAllowed) {
                return next();
            } else {
                return res.status(403).json({
                    message: 'Forbidden.'
                });
            }
        }
    });
};
