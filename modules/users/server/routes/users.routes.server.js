'use strict';

var usersPolicy = require('../policies/users.policy.server'),
    userProfile = require('../controllers/users.controllers.server');

module.exports = function (app) {

    app.route('/users/register')
        .get(userProfile.signupPage)
        .post(userProfile.signup);

    app.route('/users/login')
        .get(userProfile.signinPage)
        .post(userProfile.signin);

    app.route('/users/logout')
        .get(userProfile.signout);

    app.route('/users/create').all(usersPolicy.isAllowed)
        .get(userProfile.createPage)
        .post(userProfile.create);

    app.route('/users').all(usersPolicy.isAllowed)
        .get(userProfile.listAll);

    app.route('/users/find/:username').all(usersPolicy.isAllowed)
        .get(userProfile.find);

    app.route('/users/guests').all(usersPolicy.isAllowed)
        .get(userProfile.guests);
};
