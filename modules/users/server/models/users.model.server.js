'use strict';

var crypto = require('crypto'),
  mongoose = require('mongoose'),
  uniqueValidation = require('mongoose-beautiful-unique-validation'),
  validator = require('validator'),
  Schema = mongoose.Schema;

var passwordMinLength = 8;

var validatePassword = function (password) {
  return password && validator.isLength(password, passwordMinLength);
};

var UserSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    validate: [p => p.length != 0, 'Please fill in your first name']
  },
  lastName: {
    type: String,
    required: true,
    validate: [p => p.length != 0, 'Please fill in your last name']
  },
  displayName: {
    type: String
  },
  email: {
    type: String,
    trim: true,
    unique: 'Email exists already.',
    lowercase: true,
    required: true,
    validate: [email => validator.isEmail(email), 'Please fill a valid email address']
  },
  birthdate: {
    type: Date
  },
  gender: {
    type: String,
    enum: ['', 'female', 'male', 'non-binary', 'other'],
    default: ''
  },
  locationLiving: {
    type: String
  },
  locationFrom: {
    type: String
  },
  // Lowercase enforced username
  username: {
    type: String,
    unique: 'Username exists already.',
    required: true,
    lowercase: true,
    trim: true
  },
  password: {
    type: String,
    default: '',
    validate: [validatePassword, 'Password should be more than ' + passwordMinLength + ' characters long.']
  },
  salt: {
    type: String
  },
  roles: {
    type: [{
      type: String,
      enum: ['user', 'admin']
    }],
    default: ['user']
  },
  created: {
    type: Date,
    default: Date.now
  }
});

UserSchema.pre('save', function (next) {
  if (this.password && this.isModified('password') && this.password.length >= passwordMinLength) {
    this.salt = crypto.randomBytes(16).toString('base64');
    this.password = this.hashPassword(this.password);
  }

  // Generate `displayName`
  if (this.isModified('firstName') || this.isModified('lastName')) {
    this.displayName = this.firstName + ' ' + this.lastName;
  }

  next();
});

UserSchema.methods.hashPassword = function (password) {
  if (this.salt && password) {
    return crypto.pbkdf2Sync(password, Buffer.from(this.salt, 'base64'), 10000, 64, 'SHA1').toString('base64');
  } else {
    return password;
  }
};

UserSchema.methods.authenticate = function (password) {
  return this.password === this.hashPassword(password);
};

UserSchema.plugin(uniqueValidation);

UserSchema.index({ username: 'text', firstName: 'text', lastName: 'text' });

mongoose.model('User', UserSchema);
