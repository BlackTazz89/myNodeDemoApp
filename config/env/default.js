module.exports = {
    app: {
        title: 'Demo app'
    },
    db: {
        uri: 'mongodb://127.0.0.1/mongoosedbtest'
    },
    port: 3000,
    host: 'localhost',
    sessionSecret: 'DemoApp',
    sessionCollection: 'sessions'
};  