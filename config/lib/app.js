'use strict';

var config = require('../config'),
  mongoose = require('./mongoose'),
  express = require('./express'),
  chalk = require('chalk');

module.exports.init = function init(callback) {

  mongoose.connect(function (connection) {
    // Initialize express
    var app = express.init(connection);
    if (callback) callback(app, connection, config);
  });
};

module.exports.start = function start(callback) {

  var self = this;

  self.init(function (app, db, config) {

    app.listen(config.port, config.host, function () {

      // Logging initialization
      console.log(chalk.white('--'));
      console.log(chalk.green(new Date()));
      console.log(chalk.green('Environment:\t\t' + process.env.NODE_ENV));
      console.log(chalk.green('Database:\t\t' + config.db.uri));
      console.log(chalk.green('Database autoindexing:\t' + (config.db.autoIndex ? 'on' : 'off')));
      console.log(chalk.green('Port:\t\t\t' + config.port));

      // Reset console color
      console.log(chalk.white('--'));
      console.log('');
      console.log(chalk.white('Application is up and running now.'));
      console.log('');

      if (callback) callback(app, db, config);
    });

  });

};