'use strict';

var config = require('../config'),
  async = require('async'),
  path = require('path'),
  log = require('./logger'),
  mongoose = require('mongoose');

// Using connections global settings to avoid "Deprecated" warnings
var mongoConnectionOptions = {
  numberOfRetries: Number.MAX_SAFE_INTEGER,
  promiseLibrary: global.Promise,
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  autoIndex: Boolean(config.db.autoIndex)
};

// Load the mongoose models
module.exports.loadModels = function (callback) {
  log('info', 'Loading Mongoose Schemas.', {
    autoIndex: mongoConnectionOptions.autoIndex
  });

  // Loading models from config
  config.files.server.models.forEach(function (modelPath) {
    require(path.resolve(modelPath));
  });

  if (callback) {
    callback();
  }
};

// Initialize Mongoose
module.exports.connect = function (callback) {
  var self = this;

  mongoose.set('debug', Boolean(config.db.debug));

  async.waterfall([
    // Connect
    function (done) {
      mongoose.connect(config.db.uri, mongoConnectionOptions, function (err) {
        if (err) {
          log('error', 'Could not connect to MongoDB!', {
            error: err
          });
        }
        done(err);
      });
    },
    // Load models
    function (done) {
      self.loadModels(function () {
        done();
      });
    }
  ],
    function () {
      if (callback) {
        callback(mongoose.connection);
      }
    });
};

module.exports.disconnect = function (callback) {
  mongoose.disconnect(function (err) {
    log('info', 'Disconnected from MongoDB.');
    if (callback) {
      callback(err);
    }
  });
};