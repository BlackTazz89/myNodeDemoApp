'use strict';

var config = require('../config'),
    express = require('express'),
    pretty = require('express-prettify'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session),
    favicon = require('serve-favicon'),
    compress = require('compression'),
    cookieParser = require('cookie-parser'),
    createError = require('http-errors'),
    path = require('path');

module.exports.initMiddleware = function (app) {

    app.use(compress({
        filter: function (req, res) {
            return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'));
        },
        level: 9
    }));

    app.use(favicon('public/favicon.ico'));
    app.use(pretty({ query: 'pretty' }));

    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
        app.set('view cache', false);
    } else if (process.env.NODE_ENV === 'production') {
        app.locals.cache = 'memory';
    }

    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json({
        type: ['json']
    }));

    app.use(cookieParser());
};

module.exports.initViewEngine = function (app) {
    app.set('view engine', 'pug');
    app.set('views', './views');
};

module.exports.initSession = function (app, connection) {
    app.use(session({
        saveUninitialized: false,
        resave: true,
        secret: config.sessionSecret,
        cookie: {
            secure: false,
            maxAge: 2419200000
        },
        store: new MongoStore({
            mongooseConnection: connection,
            collection: config.sessionCollection
        })
    }));
};

module.exports.initModulesConfiguration = function (app, db) {
    config.files.server.configs.forEach(function (configPath) {
        require(path.resolve(configPath))(app, db);
    });
};

module.exports.initModulesClientRoutes = function (app) {
    app.use('/', express.static(path.resolve('./public')));
};

module.exports.initModulesServerPolicies = function () {
    config.files.server.policies.forEach(function (policyPath) {
        require(path.resolve(policyPath)).invokeRolesPolicies();
    });
};

module.exports.initModulesServerRoutes = function (app) {
    config.files.server.routes.forEach(function (routePath) {
        require(path.resolve(routePath))(app);
    });
};

module.exports.initErrorRoutes = function (app) {
    app.use(function (req, res, next) {
        next(createError(404));
    });

    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });
};

module.exports.init = function (connection) {
    var app = express();

    this.initMiddleware(app);
    this.initViewEngine(app);
    this.initModulesClientRoutes(app);
    this.initSession(app, connection);
    this.initModulesConfiguration(app);
    this.initModulesServerPolicies(app);
    this.initModulesServerRoutes(app);
    this.initErrorRoutes(app);

    return app;
};
