'use strict';

var _ = require('lodash'),
    glob = require('glob'),
    path = require('path');

// Utility function to get file by GLOB pattern
var getGlobbedPaths = function (globPatterns, excludes) {

    var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

    var output = [];

    if (_.isArray(globPatterns)) {
        globPatterns.forEach(function (globPattern) {
            output = _.union(output, getGlobbedPaths(globPattern, excludes));
        });
    } else if (_.isString(globPatterns)) {
        if (urlRegex.test(globPatterns)) {
            output.push(globPatterns);
        } else {
            var files = glob.sync(globPatterns);
            if (excludes) {
                files = files.map(function (file) {
                    if (_.isArray(excludes)) {
                        for (var i in excludes) {
                            if (excludes.hasOwnProperty(i)) {
                                file = file.replace(excludes[i], '');
                            }
                        }
                    } else {
                        file = file.replace(excludes, '');
                    }
                    return file;
                });
            }
            output = _.union(output, files);
        }
    }

    return output;
};

var initGlobalConfigFiles = function (config, assets) {
    config.files = {
        server: {}
    };

    // Models
    config.files.server.models = getGlobbedPaths(assets.server.models);

    // Routes
    config.files.server.routes = getGlobbedPaths(assets.server.routes);

    // Config
    config.files.server.configs = getGlobbedPaths(assets.server.config);

    // Policies
    config.files.server.policies = getGlobbedPaths(assets.server.policies);
};

// Initialize global config
var initGlobalConfig = function () {

    var config = require(path.join(process.cwd(), 'config/env/default'));
    var assets = require(path.join(process.cwd(), 'config/assets/default'));
    initGlobalConfigFiles(config, assets);

    // Expose utility function in config
    config.utils = {
        getGlobbedPaths: getGlobbedPaths
    };

    return config;
};

module.exports = initGlobalConfig();
