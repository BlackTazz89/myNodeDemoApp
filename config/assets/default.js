'use strict';

module.exports = {
    server: {
        models: 'modules/*/server/models/**/*.js',
        routes: 'modules/*/server/routes/**/*.js',
        config: 'modules/*/server/config/*.js',
        policies: 'modules/*/server/policies/*.js'
    }
};